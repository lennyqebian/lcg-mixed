package lcgmixed;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author leonardoquevedo
 */
public class RandomGenerator {

    protected double a;
    
    protected double x_0;
    
    protected double b;
    
    protected double m;

    public RandomGenerator(double a, double b, double m, double x_0) throws Exception {
        this.a = a;
        this.x_0 = x_0;
        this.b = b;
        this.m = m;
    }

    public LinkedHashMap random(int n) throws Exception {
        if (!(n > 0)) {
            throw new Exception("¡La cantidad de números aleatorios a generar debe ser mayor que cero (0)!");
        }
        ArrayList<Double> X = new ArrayList();
        double x_next;
        LinkedHashMap U = new LinkedHashMap(n);
        X.add(this.x_0);

        for (int i = 0; i < n; i++) {
            x_next = (this.a * X.get(i) + this.b) % this.m;
            X.add(x_next);
            U.put(x_next, x_next / this.m);
        }
        return U;
    }

    public String randomStr(int n, int decimalPlaces) throws Exception {
        LinkedHashMap U = this.random(n);
        String format;

        if (!(decimalPlaces > 1)) {
            throw new Exception("¡El número de dígitos decimales debería ser por lo menos 2!");
        }
        format = "%." + decimalPlaces + "f";

        if (U.size() > 1) {
            List<String> output = new ArrayList<>();
            Iterator iterator = U.entrySet().iterator();
            output.add("X => U");
            
            while (iterator.hasNext()) {
                Map.Entry pair = (Map.Entry) iterator.next();
                output.add(pair.getKey() + " => " + String.format(format, pair.getValue()));
            }
            return "Números aleatorios generados: \n\n"
                    + String.join("\n", output);
        }
        Object first_key = U.keySet().toArray()[0];
        return "Número aleatorio generado: " + String.format(format, U.get(first_key));
    }

    public String randomStr(int n) throws Exception {
        return randomStr(n, 4);
    }

    public LinkedHashMap random() throws Exception {
        return this.random(1);
    }
}
